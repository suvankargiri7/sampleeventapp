import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }


  createUser(user: any): Observable<HttpResponse<any[]>> {
    return this.http.post<any>('http://localhost:3002/user/signup', user);
  }

  loginUser(user: any): Observable<HttpResponse<any[]>> {
    return this.http.post<any>('http://localhost:3002/user/login', user);
  }
}
