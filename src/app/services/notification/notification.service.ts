import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient) { }


  createNotification(message: any): Observable<HttpResponse<any[]>> {
    return this.http.post<any>('http://localhost:3002/notification/create', message);
  }

  getNotificationsByUserId(userId:number): Observable<HttpResponse<any[]>> {
    return this.http.get<any[]>('http://localhost:3002/notification/user/'+userId,  { observe: 'response' });
  }
}
