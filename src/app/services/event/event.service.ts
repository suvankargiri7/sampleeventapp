import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) { 
    
  }

  getAllEvents(): Observable<HttpResponse<any[]>> {
    return this.http.get<any[]>('http://localhost:3002/event/all',  { observe: 'response' });
  }

  getEvent(id: number): Observable<HttpResponse<any[]>> {
    return this.http.get<any[]>('http://localhost:3002/event/'+id,  { observe: 'response' });
  }

  getParticipants(id:number): Observable<HttpResponse<any[]>> {
    return this.http.get<any[]>('http://localhost:3002/event/'+id+'/participants',  { observe: 'response' });
  }

  getEventsByUserId(userId:number): Observable<HttpResponse<any[]>> {
    return this.http.get<any[]>('http://localhost:3002/event/my/'+userId,  { observe: 'response' });
  }
  createEvent(event: any): Observable<HttpResponse<any[]>> {
    return this.http.post<any>('http://localhost:3002/event/create', event);
  }

  joinEvent(event: any): Observable<HttpResponse<any[]>> {
    return this.http.post<any>('http://localhost:3002/event/join', event);
  }

  likeEvent(event: any): Observable<HttpResponse<any[]>> {
    return this.http.post<any>('http://localhost:3002/event/like', event);
  }

  dislikeEvent(event: any): Observable<HttpResponse<any[]>> {
    return this.http.post<any>('http://localhost:3002/event/dislike', event);
  }

  removeParticipant(event:any): Observable<HttpResponse<any[]>> {
    return this.http.post<any>('http://localhost:3002/event/participant/remove', event);
  }

  removeEvent(id: number): Observable<HttpResponse<any[]>> {
    return this.http.get<any[]>('http://localhost:3002/event/'+id+"/remove",  { observe: 'response' });
  }
}
