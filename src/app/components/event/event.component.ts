import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder  } from '@angular/forms';
import * as moment from 'moment';


import { EventService } from '../../services/event/event.service';
import { NotificationService } from '../../services/notification/notification.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  constructor( 
    private eventService: EventService,
    private builder: FormBuilder,
    private notificationService: NotificationService
    ) { }

  allEvents: any[]=[];
  allMyEvents: any[] = [];
  allMyNotification: any[] = [];
  showMyEventsBlock: boolean = false;
  showNotificatioBlock: boolean = false;
  showAddEventForm: boolean = false;
  currentUser: number;
  eventName = new FormControl('')
  eventDescription = new FormControl('')
  eventStartdate = new FormControl('')
  eventEnddate = new FormControl('')

  addNewEventForm: FormGroup = this.builder.group({
    eventName: this.eventName,
    eventDescription: this.eventDescription,
    eventStartdate: this.eventStartdate,
    eventEnddate: this.eventEnddate
  });

  ngOnInit() {
    this.getAllEvents();
    const sessionData = JSON.parse(sessionStorage.getItem('loggedinUser'));
    this.currentUser = sessionData['id'];
    this.notificationService.getNotificationsByUserId(this.currentUser).subscribe(response => {
      console.log(response);
      this.allMyNotification = response.body;
    })
  }
  
  getAllEvents() {
     this.eventService.getAllEvents().subscribe(resp => {
       for (const data of resp.body) {
        this.allEvents.push(data);
      }
      console.log(this.allEvents);
     })
  }

  onSubmit(){
    console.log(this.addNewEventForm.value);
    const postData = {
      name : this.addNewEventForm.value.eventName,
      owner : this.currentUser,
      description : this.addNewEventForm.value.eventDescription,
      startdatetime : moment(this.addNewEventForm.value.eventStartdate).format('YYYY-MM-DD HH:MM:SS'),
      enddatetime : moment(this.addNewEventForm.value.eventEnddate).format('YYYY-MM-DD HH:MM:SS')
    };
    this.eventService.createEvent(postData).subscribe(resp => {
      console.log(resp);
      if(!resp['error']) {
        this.eventService.getEvent(resp['data']).subscribe(response => {
              console.log(response);
              if(response.body.length > 0) {
                this.updateList(response.body[0]);
              }
        });
        const notification_msg = {
          user_id: this.currentUser,
          notification_msg: "You created an event",
          created_at: moment().format('YYYY-MM-DD HH:MM:SS')
        };
        this.notificationService.createNotification(notification_msg).subscribe(notifiResponse => {
          if(!notifiResponse['error']) {
            console.log('Notification created-->',notifiResponse.body);
          }
        })
        
      }
    })
  }

  updateList(insertedEvent) {
    this.allEvents.push(insertedEvent);
  }

  getAllMyEvents() {
    if(this.showMyEventsBlock) {
      this.showMyEventsBlock = false;
    }
    else {
      this.showMyEventsBlock = true;
      this.eventService.getEventsByUserId(this.currentUser).subscribe(response => {
        this.allMyEvents=response.body;
      });
    }
    
  }

  getAllMyNotification() {
    if(this.showNotificatioBlock) {
      this.showNotificatioBlock = false;
    }
    else {
      this.showNotificatioBlock = true;
      this.notificationService.getNotificationsByUserId(this.currentUser).subscribe(response => {
        console.log(response);
        this.allMyNotification = response.body;
      })
    }
  }
  
}
