import { Component, OnInit, OnDestroy  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import * as moment from 'moment';


import { EventService } from '../../services/event/event.service';
import { NotificationService } from '../../services/notification/notification.service';

@Component({
  selector: 'app-eventdetails',
  templateUrl: './eventdetails.component.html',
  styleUrls: ['./eventdetails.component.css']
})
export class EventdetailsComponent implements OnInit, OnDestroy  {

  id: number;
  eventDetails: object = {};
  currentUserEvent: object = {
    user_like: 0,
    user_dislike: 0,
    user_joined: 0
  };
  eventPartcipants: object[] = [];
  private sub:any;
  constructor(private route: ActivatedRoute,
    private eventService: EventService,
    private builder: FormBuilder,
    private notificationService: NotificationService) { }

  currentUser: number;
 
  joineeName = new FormControl('')
  joineeEmailaddress = new FormControl('')
  joineeForm: FormGroup = this.builder.group({
    joineeName: this.joineeName,
    joineeEmailaddress: this.joineeEmailaddress
  });

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
      this.getEventDetails(this.id);
      this.getParticipants(this.id);
    });
    const sessionData = JSON.parse(sessionStorage.getItem('loggedinUser'));
    this.currentUser = sessionData['id'];
  }

  getEventDetails(id) {
    this.eventService.getEvent(id).subscribe(resp => {
       this.eventDetails = resp.body[0];
       console.log(this.eventDetails);
    })
  }

  getParticipants(id) {
    this.eventService.getParticipants(id).subscribe(response => {
      this.eventPartcipants = response.body;
      this.getCurrentuserEventRelation();
    });
  }

  getCurrentuserEventRelation() {
      for (let index = 0; index < this.eventPartcipants.length; index++) {
        const element = this.eventPartcipants[index];
        if(element['user_id'] === this.currentUser) {
          this.currentUserEvent['user_like'] = element['user_like'];
          this.currentUserEvent['user_dislike'] = element['user_dislike'];
          this.currentUserEvent['user_joined'] = element['user_joined'];
        }
      }
      console.log(this.currentUserEvent);
  }

  joinEvent() {
    const postData = {
      event_id: this.id,
      loggedin_user_id: this.currentUser
    }

    console.log(postData);
    this.eventService.joinEvent(postData).subscribe(response => {
      console.log(response);
      const sessionData = JSON.parse(sessionStorage.getItem('loggedinUser'));
      const notification_msg = {
        user_id: this.currentUser,
        notification_msg: "You joined "+this.eventDetails['event_title'],
        created_at: moment().format('YYYY-MM-DD HH:MM:SS')
      };
      this.notificationService.createNotification(notification_msg).subscribe(notifiResponse => {
        if(!notifiResponse['error']) {
          console.log('Notification created-->',notifiResponse.body);
        }
      })
      
      const notification_msg1 = {
        user_id: this.eventDetails['owner_id'],
        notification_msg: sessionData['name']+" joined "+this.eventDetails['event_title'],
        created_at: moment().format('YYYY-MM-DD HH:MM:SS')
      };
      this.notificationService.createNotification(notification_msg1).subscribe(notifiResponse => {
        if(!notifiResponse['error']) {
          console.log('Notification created-->',notifiResponse.body);
        }
      })
    });
  }

  likeEvent() {
    const postData = {
      event_id: this.id,
      loggedin_user_id: this.currentUser
    }

    console.log(postData);
    this.eventService.likeEvent(postData).subscribe(response => {
      console.log(response);
      const sessionData = JSON.parse(sessionStorage.getItem('loggedinUser'));
      const notification_msg = {
        user_id: this.currentUser,
        notification_msg: "You liked "+this.eventDetails['event_title'],
        created_at: moment().format('YYYY-MM-DD HH:MM:SS')
      };
      this.notificationService.createNotification(notification_msg).subscribe(notifiResponse => {
        if(!notifiResponse['error']) {
          console.log('Notification created-->',notifiResponse.body);
        }
      })
      const notification_msg1 = {
        user_id: this.eventDetails['owner_id'],
        notification_msg: sessionData['name']+" liked "+this.eventDetails['event_title'],
        created_at: moment().format('YYYY-MM-DD HH:MM:SS')
      };
      this.notificationService.createNotification(notification_msg1).subscribe(notifiResponse => {
        if(!notifiResponse['error']) {
          console.log('Notification created-->',notifiResponse.body);
        }
      })
    });
  }

  dislikeEvent() {
    const postData = {
      event_id: this.id,
      loggedin_user_id: this.currentUser
    }

    console.log(postData);
    this.eventService.dislikeEvent(postData).subscribe(response => {
      console.log(response);
      const sessionData = JSON.parse(sessionStorage.getItem('loggedinUser'));
      const notification_msg = {
        user_id: this.currentUser,
        notification_msg: "You disliked "+this.eventDetails['event_title'],
        created_at: moment().format('YYYY-MM-DD HH:MM:SS')
      };
      this.notificationService.createNotification(notification_msg).subscribe(notifiResponse => {
        if(!notifiResponse['error']) {
          console.log('Notification created-->',notifiResponse.body);
        }
      })
      const notification_msg1 = {
        user_id: this.eventDetails['owner_id'],
        notification_msg: sessionData['name']+" disliked "+this.eventDetails['event_title'],
        created_at: moment().format('YYYY-MM-DD HH:MM:SS')
      };
      this.notificationService.createNotification(notification_msg1).subscribe(notifiResponse => {
        if(!notifiResponse['error']) {
          console.log('Notification created-->',notifiResponse.body);
        }
      })
    });
  }

  removeParticipant(user_id) {
    const postData = {
      event_id: this.id,
      selected_user_id: user_id
    }
    
    this.eventService.removeParticipant(postData).subscribe(response => {
      console.log(response);
      this.eventPartcipants.splice(this.eventPartcipants.findIndex(v => v['user_id'] === this.currentUser), 1);
    })
  }

  leaveEvent(){
    const postData = {
      event_id: this.id,
      selected_user_id: this.currentUser
    }
    
    this.eventService.removeParticipant(postData).subscribe(response => {
      console.log(response);
      const sessionData = JSON.parse(sessionStorage.getItem('loggedinUser'));
      const notification_msg = {
        user_id: this.currentUser,
        notification_msg: "You left "+this.eventDetails['event_title'],
        created_at: moment().format('YYYY-MM-DD HH:MM:SS')
      };
      this.notificationService.createNotification(notification_msg).subscribe(notifiResponse => {
        if(!notifiResponse['error']) {
          console.log('Notification created-->',notifiResponse.body);
        }
      })
      const notification_msg1 = {
        user_id: this.eventDetails['owner_id'],
        notification_msg: sessionData['name']+" left "+this.eventDetails['event_title'],
        created_at: moment().format('YYYY-MM-DD HH:MM:SS')
      };
      this.notificationService.createNotification(notification_msg1).subscribe(notifiResponse => {
        if(!notifiResponse['error']) {
          console.log('Notification created-->',notifiResponse.body);
        }
      })
    })
  }

  removeEntireEvent(){
    this.eventService.removeEvent(this.id).subscribe(resp => {
      console.log(resp);
      const sessionData = JSON.parse(sessionStorage.getItem('loggedinUser'));
      const notification_msg = {
        user_id: this.currentUser,
        notification_msg: "You removed "+this.eventDetails['event_title'],
        created_at: moment().format('YYYY-MM-DD HH:MM:SS')
      };
      this.notificationService.createNotification(notification_msg).subscribe(notifiResponse => {
        if(!notifiResponse['error']) {
          console.log('Notification created-->',notifiResponse.body);
        }
      })
   })
   
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
