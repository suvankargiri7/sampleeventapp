import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.css']
})
export class TopnavComponent implements OnInit {

  @Input() appTitle: string;

  showLoggedOut: boolean = false;
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
   if(sessionStorage.getItem('loggedinUser')) {
      this.showLoggedOut = true;
   }
  }

  userLogout() {
    sessionStorage.removeItem('loggedinUser');
    this.showLoggedOut = false;
    this.router.navigate(['/']);
  }

}
