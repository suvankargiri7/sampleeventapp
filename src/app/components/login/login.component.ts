import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private builder: FormBuilder,
    private userService: UserService
  ) { }

  loginEmail = new FormControl('')
  loginPassword = new FormControl('')
  loginRememberMe = new FormControl(false)
  isChecked:boolean = false;

  loginForm: FormGroup = this.builder.group({
    loginEmail: this.loginEmail,
    loginPassword: this.loginPassword,
    loginRememberMe: this.loginRememberMe
  });

  ngOnInit() {
    this.getRememberMedata();
  }

  getRememberMedata() {
    if (localStorage.getItem('previoususer') !== null) {
      const previousUserdata = JSON.parse(localStorage.getItem('previoususer'));
      console.log(previousUserdata);
      this.isChecked = true;
      this.loginEmail = new FormControl(previousUserdata.email);
      this.loginPassword = new FormControl(previousUserdata.password);
      this.loginForm =this.builder.group({
        loginEmail: this.loginEmail,
        loginPassword: this.loginPassword,
        loginRememberMe: this.loginRememberMe
      });
    }
  }

  checkValue(event: any) {
    if (event) {
      localStorage.setItem('previoususer', JSON.stringify({
        email: this.loginForm.value.loginEmail,
        password: this.loginForm.value.loginPassword
      }));
    }
    else{
      localStorage.removeItem('previoususer');
    }
  }

  login() {
    console.log(this.loginForm.value);
    const postData = {
      email: this.loginForm.value.loginEmail,
      password: this.loginForm.value.loginPassword
    };
    this.userService.loginUser(postData).subscribe(resp => {
      sessionStorage.setItem('loggedinUser', JSON.stringify(resp));
      if (resp) {
        this.router.navigate(['/events']).then(() => {
          window.location.reload();
        });
      }
    })

  }
}
