import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder  } from '@angular/forms';
import {Router} from '@angular/router';
import * as moment from 'moment';

import { UserService } from '../../services/user/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(
    private router: Router,
    private builder: FormBuilder,
    private userService: UserService) { }


  signUpName = new FormControl('')
  signUpEmail = new FormControl('')
  signUpPassword = new FormControl('')
  signUpRePassword = new FormControl('')

  signupForm: FormGroup = this.builder.group({
    signUpName: this.signUpName,
    signUpEmail: this.signUpEmail,
    signUpPassword: this.signUpPassword,
    signUpRePassword: this.signUpRePassword
  });

  ngOnInit() {
  }

  signUp() {
    console.log(this.signupForm.value);
    const postData = {
      name : this.signupForm.value.signUpName,
      email : this.signupForm.value.signUpEmail,
      password : this.signupForm.value.signUpPassword,
    };
    this.userService.createUser(postData).subscribe(resp => {
      if(resp) {
        this.router.navigate(['/']);
      }
    })
  }
}
