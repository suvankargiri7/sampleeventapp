import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventComponent } from './components/event/event.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { EventdetailsComponent } from './components/eventdetails/eventdetails.component';

const routes: Routes = [
 {
   path: 'events',
   component: EventComponent
 },
 {
   path: '',
  component: LoginComponent
 },
 {
   path: 'register',
   component: RegisterComponent
 },
 {
   path: 'event/:id',
   component: EventdetailsComponent
 }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
